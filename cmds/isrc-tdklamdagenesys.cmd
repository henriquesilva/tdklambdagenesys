require stream,2.8.10
require tdklambdagenesys,1.0.2test

# Load CoilPS-01
iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=CoilsPS-01, IP_ADDR=172.16.60.43, P=ISrc:, R=PwrC-CoilPS-01:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")

# Load CoilPS-02
iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=CoilsPS-02, IP_ADDR=172.16.60.44, P=ISrc:, R=PwrC-CoilPS-02:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")

# Load CoilPS-03
iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=CoilsPS-03, IP_ADDR=172.16.60.45, P=ISrc:, R=PwrC-CoilPS-03:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")
