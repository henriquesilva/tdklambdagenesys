# This should be a test startup script
require stream
require tdklambdagenesys

# Load records for a LAN based power supply
# iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=ps-test, IP_ADDR=127.0.0.1, P=testsecsub:, R=testdisdevidx:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12, DEVICETYPE=tdklambdagenesysLAN, LANMODULE=1")

# Load records for a serial controlled power supply
iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=ps-test, IP_ADDR=127.0.0.1, P=testsecsub:, R=testdisdevidx:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")
